﻿using UnityEngine;
using System.Collections;

public class AreaOfEffectTower : TowerBase 
{
   
    protected override void Action(Transform target)
    {
       
        base.Action(target);

        foreach (GameObject e in SpawnManager.Instance.enemyList)
        {
            float enemyDistance = Vector3.SqrMagnitude(transform.position - e.transform.position);
            if (enemyDistance <= areaOfRange)
            {
                if (e.gameObject.GetComponent<BaseHealth>() != null)
                {
                    BaseHealth health = e.gameObject.GetComponent<BaseHealth>();
                    health.OnDamage(damage);
                }
            }
        }  
    }

    protected override void Upgrade()
    {
        // tower upgrades
    }
}
