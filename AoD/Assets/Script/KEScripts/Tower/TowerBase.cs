﻿using UnityEngine;
using System.Collections;

public abstract class TowerBase : MonoBehaviour
{
    #region parameters

    public float coolDown = 1.0f;
    public float areaOfRange;

    public int cost;

    protected float refreshSpeed = 0.2f;
    protected float lastTick;
    protected float lastAction;
    protected GameObject[] enemies;

    public float CoolDown { set { coolDown = value; } get { return coolDown; } }
    public float AreaOfRange { set { areaOfRange = value; } get { return areaOfRange; } }

    public DamageDetails damage;

    #endregion

    #region Initializaion & Update
    void Update () 
    {
        if (Time.time - lastAction > coolDown)
        {
            if (Time.time - lastTick > refreshSpeed)
            {
                lastTick = Time.time;
                Transform target = GetClosestEnemy();
                if (target)
                {
                    Action(target);

                    // PUT ROTATION SCRIPT HERE
                    //this.transform.LookAt(target);
                }
            }
        }
	}
    #endregion

    #region Functions
    Transform GetClosestEnemy()
    {
       //enemies[] = SpawnManager.Instance.enemyList;
       //Collider[] enemies = Physics.OverlapSphere(transform.position, areaOfRange, LayerMask.GetMask("Enemy"));
       // if(enemies.Length != 0)
       // {
       //     int NearestEnemyIndex = 0;
       //     float nearestEnemyDistance = Vector3.SqrMagnitude(transform.position - enemies[0].transform.position);

       //     for (int i = 1; i <enemies.Length; i++)
       //     {
       //         float tempDistance = Vector3.SqrMagnitude(transform.position - enemies[i].transform.position);
       //         {
       //             if (nearestEnemyDistance < tempDistance)
       //             {
       //                 nearestEnemyDistance = tempDistance;
       //                  nearestEnemyDistance = 1;
       //             }
       //         }
       //     }

       //     return enemies[NearestEnemyIndex].transform;
       // }

        // find nearest nemy
        float distanceDetect = Mathf.Infinity;
        Transform closestTarget = null;
        foreach (GameObject enemies in SpawnManager.Instance.enemyList)
        {
            if (enemies != null)
            {
                float curdist = Vector3.Distance(transform.position, enemies.transform.position);

                if (curdist < areaOfRange)
                {
                    if (curdist < distanceDetect)
                    {
                        distanceDetect = curdist;
                        closestTarget = enemies.transform;
                    }
                }
            }
            ////float curdist = dist.magnitude;
            //Debug.Log(enemy.name + ": " + curdist);
            //if (curdist < distance && curdist < 20)
            //{
            //    return enemy.transform;
            //}
        }
        return closestTarget;
    }

    protected virtual void Action (Transform target)
    {
        lastAction = Time.time;
    }

    protected abstract void Upgrade();
    #endregion
}
