﻿using UnityEngine;
using System.Collections;

public class ShootingTower : TowerBase 
{
    public GameObject projectile;
    public Transform barrel;
    protected override void Action(Transform target)
    {
        base.Action(target);

        GameObject bullet = Instantiate(projectile, transform.position, Quaternion.identity) as GameObject;
        bullet.GetComponent<BaseProjectile>().Shoot(barrel, target, damage);
    }

    protected override void Upgrade()
    {
        // tower upgrades
    }
}
