﻿using UnityEngine;
using System.Collections;

public abstract class BaseController : MonoBehaviour
{
    #region parameters
    protected CharacterController controller;
    protected BaseStateMachine currentState;

    public float speed;
    public float Speed { get { return speed; } }
    public Vector3 moveDirection { set; get; }
    public Quaternion moveRotation { set; get; }
    public float verticalVelovity { set; get; }
    protected abstract void UpdateController();
    #endregion
    
    #region Initilization & Update
    protected virtual void Start () 
    {
        controller = GetComponent<CharacterController>();
        currentState = GetComponent<BaseStateMachine>();
        currentState.Create();
	}
	

	void Update () 
    {
        UpdateController();
    }
    #endregion

    #region functions
    protected virtual void MoveController()
    {
        controller.Move(moveDirection * Time.deltaTime);
    }
    protected virtual void RotateController()
    {
        this.transform.rotation = moveRotation;
    }
    public void ChangeState(string state)
    {
        System.Type t = System.Type.GetType(state);

        currentState.Destroy();
        currentState = gameObject.AddComponent(t) as BaseStateMachine;
        currentState.Create();
    }
    #endregion
}
