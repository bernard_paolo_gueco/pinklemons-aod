﻿using UnityEngine;
using System.Collections;

public class EnemyController : BaseController
{
    #region Parameters
    private Vector3 targetPosition = Vector3.zero;
    #endregion

    #region Initialization & Update
    protected override void Start()
    {
        base.Start();
        
    }
    protected override void UpdateController()
    {
        moveDirection = Direction();
        moveDirection = currentState.Movement(moveDirection);
        moveRotation = currentState.Rotation(moveDirection);

        currentState.Change();

        MoveController();
        RotateController();
    }
    #endregion

    #region functions 
    public void SetTargetPosition (Transform target)
    {
        targetPosition = target.position;
    }

    public Vector3 Direction ()
    {
        if (targetPosition == Vector3.zero)
            return targetPosition;

        Vector3 dir = targetPosition - transform.position;
        dir.Set(dir.x, 0, dir.z);
        return dir.normalized;
    }
    #endregion
}
