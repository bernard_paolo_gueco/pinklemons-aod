﻿using UnityEngine;
using System.Collections;

public abstract class BaseStateMachine : MonoBehaviour 
{
    protected BaseController controller;
	
    public virtual void Create()
    {
        controller = GetComponent<BaseController>();
    }
    public virtual void Destroy()
    {
        Destroy(gameObject);
    }
    public virtual void Change()
    {

    }

    #region functions 
    public abstract Vector3 Movement(Vector3 value);
    public virtual Quaternion Rotation(Vector3 value)
    {
        return transform.rotation;
    }

    protected void Speed (ref Vector3 direction, float speed)
    {
        direction *= speed;
    }
    #endregion
}
