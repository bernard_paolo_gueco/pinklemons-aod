﻿using UnityEngine;
using System.Collections;

public class EnemyWalkState : BaseStateMachine
{
    #region Enemy Movemet 
    public override Vector3 Movement(Vector3 value)
    {
         Speed(ref value, controller.speed);
         return value;
    }
    public override Quaternion Rotation(Vector3 value)
    {
        return Quaternion.FromToRotation(Vector3.forward, value);
    }
    #endregion
 
	public override void Create () 
    {
	    base.Create();
	}
    public override void Change()
    {
        
    }
   
}
