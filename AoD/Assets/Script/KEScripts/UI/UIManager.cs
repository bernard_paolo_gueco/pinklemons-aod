﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class UIManager : Singleton<UIManager>
{
    #region parameters
    public Text wave;
    public Text enemiesRemaining;
    public Image life;
    public Text gameOver;
    public Text gold;
    public Text towerSelectedName;
    public Text towerSelectedCost;

    public Text enemiesKilled;
    public Text towersBuilt;
    public Text resultCoins;

    public List<PopUpMessage> popup = new List<PopUpMessage>();
    #endregion 
    void Start()
    {
        Gold();
        TowerClicked(0);
    }
    #region functions
    public void  UpdateText()
    {
        wave.text = "CURRENT WAVE: " + LevelManager.Instance.WaveInfo();
        enemiesRemaining.text = "ENEMIES REMAINING: " + SpawnManager.Instance.EnemyCount();
        life.fillAmount = PlayerStats.Instance.LifeDecimal();
    }


    public void Result(string text)
    {
        gameOver.text = text;
        enemiesKilled.text = "ENEMIES KILLED: " + SpawnManager.Instance.enemiesKilled.ToString();
        towersBuilt.text = "TOWERS BUILT: " + TowerBuilder.Instance.TowerCount().ToString();
        resultCoins.text = "COINS EARNED: " + PlayerStats.Instance.gold;
    }

    public void TowerClicked (int towerIndex)
    {
        TowerBuilder.Instance.currentTowerIndex = towerIndex;
        towerSelectedName.text = TowerBuilder.Instance.SelectedTowerName(); ;
        towerSelectedCost.text = "Cost: " + TowerBuilder.Instance.SelectedTowerCost();
    }

    public void Gold()
    {
        gold.text = "GOLD: " + PlayerStats.Instance.gold;
    }

    public void ShowPopUp(string message, float duration)
    {
        for (int i = 0; i < popup.Count; i++)
        {
            if (!popup[i].isActive)
            {
                popup[i].txt.text = message;
                popup[i].duration = duration;
                popup[i].lastShow = Time.time;
                popup[i].isActive = true;
                popup[i].gameObject.SetActive(true);
                popup[i].gameObject.transform.SetAsFirstSibling();
                return;
            }
        }
    }
    #endregion
}
