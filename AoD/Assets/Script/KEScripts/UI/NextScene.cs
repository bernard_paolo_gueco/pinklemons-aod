﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
public class NextScene : MonoBehaviour {

	public void Next(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
