﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PopUpMessage : MonoBehaviour 
{
    public bool isActive = false;
    public float lastShow;
    public float duration;

    public Text txt;
	// Use this for initialization
	void Start () 
    {

	}
	
	// Update is called once per frame
	void Update () 
    {
        if (!isActive)
            return;

        if (Time.time - lastShow >  duration)
        {
            isActive = false;
            gameObject.SetActive(false);
        }
	}
}
