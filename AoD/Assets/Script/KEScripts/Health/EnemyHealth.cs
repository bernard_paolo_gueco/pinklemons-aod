﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EnemyHealth : BaseHealth
{
    public GameObject HealthBar;
    public Image filledImage;

    public override void OnDamage(DamageDetails damage)
    {
		HealthBar.SetActive(true);
        filledImage.fillAmount = (float)currentHealth / (float)maxHealth;
        PlayerStats.Instance.gold += 5;
        base.OnDamage(damage);
    }
    protected override void Death()
    {
        HealthBar.SetActive(false);
        SpawnManager.Instance.RemoveEnemy(gameObject);
    }

}
