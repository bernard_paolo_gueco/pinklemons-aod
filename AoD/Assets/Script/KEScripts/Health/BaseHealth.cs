﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
[System.Serializable]
public class DamageDetails
{
    public int value;
}
public abstract class BaseHealth : MonoBehaviour
{
    #region parameters
    [SerializeField]
    protected int currentHealth;
    [SerializeField]
    protected int maxHealth = 10;
    public int CurrentHealth { set { currentHealth = value; } get { return currentHealth; } }
    public int MaxHealth { set { maxHealth = value; } get { return maxHealth; } }
    #endregion 

    #region initialization & update
    void Start () 
    {
        Initalize();
	}

	protected virtual void Initalize()
    {
        currentHealth = maxHealth;
    }
    #endregion

    #region function
    public virtual void OnDamage(DamageDetails damage)
    {
        currentHealth -= damage.value;

        if (currentHealth <= 0)
        {
            Death();
        }
    }

    protected abstract void Death();
    #endregion
}
