﻿using UnityEngine;
using System.Collections;

public class TowerHealth : BaseHealth 
{
    protected override void Death()
    {
        Destroy(gameObject);
    }
}
