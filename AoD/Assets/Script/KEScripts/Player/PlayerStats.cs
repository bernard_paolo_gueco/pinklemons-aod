﻿using UnityEngine;
using System.Collections;

public class PlayerStats : Singleton<PlayerStats>  
{
    public int gold;
    public int life;
    public int maxLife;

    public float LifeDecimal()
    {
        return (float)life / (float)maxLife;
    }
}


