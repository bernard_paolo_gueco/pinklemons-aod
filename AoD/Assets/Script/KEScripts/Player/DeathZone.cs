﻿using UnityEngine;
using System.Collections;

public class DeathZone : MonoBehaviour 
{
    void OnTriggerEnter(Collider other)
    {
       
        if (other.gameObject.tag == "Enemy")
        {
            SpawnManager.Instance.RemoveEnemy(other.gameObject);
            LevelManager.Instance.Damage();
        }
    }
}
