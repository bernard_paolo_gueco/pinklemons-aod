﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public abstract class SkillBase : MonoBehaviour
{
    #region parameters
    public float coolDown;
    protected float lastTime;

    Button button;
    #endregion

    #region Initalization and Update 
   protected virtual void Start()
    {
        button = GetComponent<Button>();
    }

    protected virtual void Update()
    {
        button.interactable = Requirement();
    }
    #endregion

    #region functions
    protected virtual bool Requirement()
    {
        if (Time.time - lastTime <= coolDown)
            return false;
        else
            return true;
    }

    protected abstract void Use();
    public void PressedButton()
    {
        if (Requirement())
        {
            Use();
            lastTime = Time.time;
        }
    }
    #endregion
}
