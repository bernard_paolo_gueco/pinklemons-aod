﻿using UnityEngine;
using System.Collections;

public class DamageAllEnemiesSkill : SkillBase 
{
    public DamageDetails damage;

    EnemyHealth[] enemies;
    protected override void Use()
    {
        enemies = GameObject.FindObjectsOfType<EnemyHealth>();
        foreach (EnemyHealth enemy in enemies)
        {
            enemy.OnDamage(damage);
        }

    }
}
