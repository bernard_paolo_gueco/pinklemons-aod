﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class BoostTowerSkill : SkillBase
{
    #region parameters
    public int multiplier = 2;
    public float buffDuration;

    TowerBase[] towers;
    #endregion

    #region functions
    protected override void Use()
    {
        towers = GameObject.FindObjectsOfType<TowerBase>();
        BoostTowers();
        StartCoroutine(BuffTimer());
    }
    IEnumerator BuffTimer()
    {
        yield return new WaitForSeconds(buffDuration);
        ResetTowers();
    }
    void BoostTowers()
    {
        foreach (TowerBase t in towers)
        {
            t.CoolDown *= multiplier;
            t.AreaOfRange *= multiplier;
        }
    }

    void ResetTowers()
    {
        foreach (TowerBase t in towers)
        {
            t.CoolDown /= multiplier;
            t.AreaOfRange /= multiplier;
        }
    }
    #endregion
}