﻿using UnityEngine;
using System.Collections;

public class WayPoint : MonoBehaviour 
{
    public Transform nextTarget;

    private void OnTriggerEnter(Collider other)
    {
        EnemyController controller = other.gameObject.GetComponent<EnemyController>();
        if (controller != null)
        {
            controller.SetTargetPosition(nextTarget);
        }
    }
}
