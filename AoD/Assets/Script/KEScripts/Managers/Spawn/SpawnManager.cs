﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Spawner
{
    public string name;
    public Transform self;
    public Transform target;
}
public class SpawnManager : Singleton<SpawnManager>
{
	public GameObject playerObject;
    public MapBuilder map;
    #region parameters
    public List<GameObject> spawnableObject = new List<GameObject>();
    public Spawner spawn;
    public List<GameObject> enemyList = new List<GameObject>();
    public int enemiesKilled;
    #endregion 

    #region functions
    public void SpawnEnemy(string spawnObject)
    {
        for (int i = 0; i < spawnableObject.Count; i++)
        {
            if (spawnObject == spawnableObject[i].name)
            {
                GameObject go = Instantiate(spawnableObject[i], spawn.self.position, spawn.self.rotation) as GameObject;
                EnemyController controller = go.GetComponent<EnemyController>();
                if (controller != null)
                {
                    controller.SetTargetPosition(spawn.target);
                    enemyList.Add(go);
                }

                UIManager.Instance.UpdateText();
             }
        }
    }
    public Transform GetEnemy (int index)
    {
        return enemyList[index].transform;
    }
    public void RemoveEnemy(GameObject enemy)
    {
        enemyList.Remove(enemy);
        enemiesKilled++;
        Destroy(enemy);
        UIManager.Instance.UpdateText();
		UIManager.Instance.Gold ();
    }
    public int EnemyCount()
    {
        return enemyList.Count;
    }
    #endregion
}
