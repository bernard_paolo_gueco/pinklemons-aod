﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
[System.Serializable]
#region Wave Event
public class WaveEvent
{
    #region parameters
    public string name;
    public float duration;
    private float startTime;
    public List <SpawnInformation> Spawners;
    #endregion

    #region initialization & update
    public void Start()
    {
        startTime = Time.time;
    }
    #endregion

    #region function
    public bool Run()
    {
        if (duration == 0.0f && Spawners.Count == 0)  
            return false;
        else if (Time.time - startTime > duration && duration != 0.0f)
            return false;

        for (int x = 0; x < Spawners.Count; x++)
        {
            Spawners[x].Spawn();

            if (Spawners[x].amount == 0)
            {
                Spawners.RemoveAt(0);
            }
        }

        return true;
    }
    #endregion
}
#endregion

[System.Serializable]
#region Spawn Information
public class SpawnInformation
{
    #region parameters
    public string name;
    public string spawnName;
    public int amount = 5;
    public float intervalTime;

    private float lastTime;
    #endregion
    public void Spawn()
    {
        if (Time.time - lastTime >= intervalTime)
        {
            SpawnManager.Instance.SpawnEnemy(spawnName);
            amount --;
            lastTime = Time.time;
        }
    }
}
#endregion
public class Wave : MonoBehaviour
{
    #region parameters
    bool isActive = false;
    public List<WaveEvent> waveEvents = new List<WaveEvent>();
    #endregion

    #region Initialization and Update
    public void StartWave()
    {
        isActive = true;

        if (waveEvents.Count != 0)
            waveEvents[0].Start();
        else
            LevelManager.Instance.EndWave();
    }
	
    // Update is called once per frame
    void Update () 
    {
        if (!isActive)
            return;
	    if (!waveEvents[0].Run())
        {
            waveEvents.RemoveAt(0);
            if (waveEvents.Count == 0)
            {
                LevelManager.Instance.EndWave();
            }
            else
                waveEvents[0].Start();
        }

    }
    #endregion
}
