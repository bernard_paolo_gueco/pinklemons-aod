﻿using UnityEngine;
using System.Collections;
using System.Text;
using System.IO;

public class MapBuilder : MonoBehaviour 
{
    public string fileNameToLoad;

    public int mapWidth;
    public int mapHeight;

    public int xOffset;
    public int yOffset;

    public GameObject ImpassableTile;
    public GameObject BuildableTile;
    public GameObject PassableTile;

    private char[,] tiles;
	// Use this for initialization
	void Awake () 
    {
        tiles = Load(Application.dataPath + "\\" + fileNameToLoad);
        BuildMap();
	}

    void BuildMap()
    {
        GameObject tile;
        Debug.Log("Building Map...");
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            for (int j = 0; j < tiles.GetLength(1); j++)
            {
                switch(tiles[i,j])
                {
                    case 'i':
                        tile = Instantiate(ImpassableTile, new Vector3((j - mapWidth) + xOffset, 0, (mapHeight - i) + yOffset), Quaternion.identity) as GameObject;
                        tile.transform.SetParent(gameObject.transform);
                    break;
                    case 'b':
                        tile = Instantiate(BuildableTile, new Vector3((j - mapWidth) + xOffset, 0, (mapHeight - i) + yOffset), Quaternion.identity) as GameObject;
                        tile.transform.SetParent(gameObject.transform);
                    break;
                    case 'p':
                    case 's':
                    case 'g':
                        tile = Instantiate(PassableTile, new Vector3((j - mapWidth) + xOffset, 0, (mapHeight - i) + yOffset), Quaternion.identity) as GameObject;
                        tile.transform.SetParent(gameObject.transform);
                    break;
                    
                }
            }
        }
        Debug.Log("Building Completed!");
    }

    private char[,] Load(string filePath)
    {
        try
        {
            Debug.Log("Loading File...");
            using (StreamReader sr = new StreamReader(filePath))
            {
                string input = sr.ReadToEnd();
                string[] lines = input.Split(new[] { '\r', '\n' }, System.StringSplitOptions.RemoveEmptyEntries);
                char[,] tiles = new char[lines.Length, mapWidth];
                Debug.Log("Parsing...");
                for (int i = 0; i < lines.Length; i++)
                {
                    string st = lines[i];
                    string[] nums = st.Split(new[] { ' ' });
                    if (nums.Length != mapWidth)
                    {

                    }
                    for (int j = 0; j < Mathf.Min(nums.Length, mapWidth); j++)
                    {
                        char val;
                        if (char.TryParse(nums[j], out val))
                        {
                            tiles[i, j] = val;
                        }
                        else
                        {
                            Debug.Log("INVALID TILE");
                        }
                    }
                }
                Debug.Log("Parsing Completed!");
                return tiles;
            }
        }
        catch (IOException e)
        {
            //Debug.Log(e.Message);
        }
        return null;
    }
}
