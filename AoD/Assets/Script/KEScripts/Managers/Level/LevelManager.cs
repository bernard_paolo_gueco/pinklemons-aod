﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class LevelManager : Singleton<LevelManager>
{
    #region parameters
    public int currentWave;
    public int AmmountOfWave;

    bool spawnIsActive = false;
    public bool waveIsActive = false;
    bool isActive = false;
    public List<Wave> listOfWaves = new List<Wave>();

    public GameObject result;

    #endregion 
    #region Initalization and Update
    void Start()
    {
        currentWave = 0;
        AmmountOfWave = listOfWaves.Count;

        UIManager.Instance.UpdateText();
    }
	// Update is called once per frame
    void Update()
    {
        if (!isActive)
            return;
        if (!spawnIsActive && !GameObject.FindGameObjectWithTag("Enemy") && isActive)
        {
            waveIsActive = false;
            isActive = false;
            UIManager.Instance.ShowPopUp("FINISHED WAVE", 2.0f);
            if (listOfWaves.Count == 0)
            {
                WinGame();
            }
        }
    }
    #endregion

    #region hit and death
    public void Damage()
    {
        PlayerStats.Instance.life--;
        UIManager.Instance.UpdateText();
        if (PlayerStats.Instance.life == 0)
        {
            LevelManager.Instance.GameOver();
        }

        if (PlayerStats.Instance.life <= 0)
        {
            PlayerStats.Instance.life = 0;
        }
    }
    public void GameOver()
    {
        result.SetActive(true);
        UIManager.Instance.Result("GAME OVER");
        Time.timeScale = 0;
    }
    public void WinGame()
    {
        result.SetActive(true);
        UIManager.Instance.Result("YOU WIN!");
        Time.timeScale = 0;
    }
    
    #endregion

    #region wave 
    public void StartWave()
    {
        if (!waveIsActive && listOfWaves.Count != 0)
        {
            currentWave++;
            listOfWaves[0].StartWave();
            spawnIsActive = true;
            waveIsActive = true;
            isActive = true;
            UIManager.Instance.UpdateText();
        }
    }

    public void EndWave()
    {
        Destroy(listOfWaves[0]);
        listOfWaves.RemoveAt(0);
        spawnIsActive = false;
        UIManager.Instance.UpdateText();
        UIManager.Instance.ShowPopUp("LAST SET OF ENEMIES", 2.0f);
    }
    public void FinishedLevel()
    {
        Debug.Log("End of Level");
    }

    public string WaveInfo()
    {
       return currentWave.ToString() + " /" + AmmountOfWave.ToString();
    }

    #endregion
}
