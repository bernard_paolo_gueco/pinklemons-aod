﻿using UnityEngine;
using System.Collections;

public class ShootingProjectile : BaseProjectile 
{
    public override void Shoot(Transform tower, Transform target, DamageDetails damageValue)
    {
        base.Shoot(tower, target, damageValue);
        lockOnTargetEnemy=  true;
    }
}
