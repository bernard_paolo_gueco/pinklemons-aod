﻿using UnityEngine;
using System.Collections;

public  abstract class  BaseProjectile : MonoBehaviour 
{
    public Transform currentTower;
    public Transform targetEnemy;

    protected Vector3 targetLocation;

    protected DamageDetails damage;
    public float coolDown = 10f;
    public float speed = 2f;

    protected float curPos = 0.0f;
    protected bool lockOnTargetEnemy = false;
    protected bool isShooting;

	protected virtual void Update () 
    {
	    if (isShooting)
        {
            curPos += Time.deltaTime / speed;
            if (lockOnTargetEnemy && targetEnemy)
            {
                targetLocation = targetEnemy.position;
            }
            if (curPos > 1)
            {
                Destroy(gameObject);
            }
            transform.position = Vector3.Lerp(currentTower.position, targetLocation, curPos);
        }
	}

    public virtual void Shoot(Transform tower, Transform target, DamageDetails damageValue)
    {
        isShooting = true;
        targetEnemy = target;
        currentTower = tower;
        targetLocation = target.position;
        damage = damageValue;
    }

    public void DamageTarget(GameObject target)
    {
        BaseHealth health = targetEnemy.GetComponent<BaseHealth>();
        health.OnDamage(damage);

    }
   //  reach the target enemy
    void OnTriggerEnter(Collider other)
    {
        if (targetEnemy != null)
        {
            DamageTarget(other.gameObject);
        }
    }
}
