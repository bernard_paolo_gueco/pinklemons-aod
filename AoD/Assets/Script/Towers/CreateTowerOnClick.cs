﻿using UnityEngine;
using System.Collections;

public class CreateTowerOnClick : MonoBehaviour {

	public GameObject playerObject;
    Ray ray;
    RaycastHit hit;
	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		//Set up our ray from screen to scene;
		ray = Camera.main.ScreenPointToRay (Input.mousePosition);

		if (Input.GetMouseButtonDown (0)) 
		{  //Check to see if we've clicked
			//If we hit...
				if (Physics.Raycast (ray, out hit) && hit.transform.gameObject.tag == "Platform") {

                    TowerPlacement tower = hit.transform.gameObject.GetComponent<TowerPlacement>();
                    TowerBuilder.Instance.Build(ref tower);
				}			
		}

		if (Input.GetMouseButtonDown (1)) 
		{
			if (Physics.Raycast (ray, out hit) && hit.transform.gameObject.tag == "Tower") {
				Destroy (hit.transform.gameObject);
			}	
		}
			

	
	}
}
