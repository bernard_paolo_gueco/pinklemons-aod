﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
public class TowerBuilder : Singleton<TowerBuilder>  
{
    public GameObject playerObject;
    public GameObject gameplayMode;
    public GameObject buildMode;
	public Image builderButton;

	public Sprite build;
	public Sprite endBuild;

    public List<TowerBase> tower = new List<TowerBase>();
    public List<GameObject> towerList = new List<GameObject>();

    public int currentTowerIndex;
   public void Build(ref TowerPlacement placement)
	{
        // enough coins
        Vector3 towerPosition = new Vector3(placement.transform.position.x, placement.transform.position.y + 0.5f, placement.transform.position.z);
        if (SelectedTowerCost() <= PlayerStats.Instance.gold)
        {
            PlayerStats.Instance.gold -= SelectedTowerCost();
            UIManager.Instance.Gold();
            //if slot is available
            if (!placement.taken)
            {
                GameObject currentTower = Instantiate(tower[currentTowerIndex].gameObject, towerPosition, Quaternion.identity) as GameObject;
                towerList.Add(currentTower);
                placement.taken = true;
                placement.tower = currentTower;
            }

            //tower is already placed
            else if (placement.taken)
            {
                UIManager.Instance.ShowPopUp("TAKEN!", 2.0f);
            }
        }

        else
        {
            UIManager.Instance.ShowPopUp("NOT ENOUGH COINS!", 2.0f);
        }

	}

    public void RemoveTower(GameObject tower)
    {
        towerList.Remove(tower);
        Destroy(tower);
    }

    public int TowerCount()
    {
        return towerList.Count;
    }
	
	public int SelectedTowerCost()
    {
        return tower[currentTowerIndex].cost;
    }
    public string SelectedTowerName()
    {
        return tower[currentTowerIndex].name;
    }
}
