﻿using UnityEngine;
using System.Collections;

public class CreateTowerOnClicked : MonoBehaviour
{
	public TowerSelector towerSelector;
	
	void Clicked(Vector3 position)
	{
		GameObject tower = towerSelector.GetSelectedTower();
		Instantiate(tower,position + Vector3.up*0.5f,tower.transform.rotation);
	}
}
