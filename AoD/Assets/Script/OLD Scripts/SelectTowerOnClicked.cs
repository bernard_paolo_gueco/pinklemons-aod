﻿using UnityEngine;
using System.Collections;

public class SelectTowerOnClicked : MonoBehaviour {
	
	public GameObject TowerSelector;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void Clicked()
	{
		//Tell the tower selector to change the selected tower
		TowerSelector.SendMessage("SetSelectedTower", gameObject);
	}
}
